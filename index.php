<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <style>
        .card{
            padding:10px
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="card">
            <div class="row">
                <div class="col-md-6">
                    <h2>Tambah Data Mahasiswa</h2>
                    <form id="addNewData">
                        <div class="form-group">
                            <label>Nama Mahasiswa</label>
                            <input type="text" name="nama" class="form-control" placeholder="Nama Mahasiswa" required>
                        </div>
                        <div class="form-group">
                            <label>No HP</label>
                            <input type="text" name="hp" class="form-control" placeholder="No HP" required>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Alamat Email" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-info">Tambah Data</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <h2>Edit Data Mahasiswa</h2>
                    <form id="editData">
                        <div class="form-group">
                            <label>Nama Mahasiswa</label>
                            <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Mahasiswa">
                        </div>
                        <div class="form-group">
                            <label>No HP</label>
                            <input type="text" name="hp" id="hp" class="form-control" placeholder="No HP">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Alamat Email">
                        </div>
                        <div class="form-group">
                            <button type="reset" class="btn btn-sm btn-secondary">Batal</button>
                            <button type="submit" class="btn btn-sm btn-info">Update Data</button>
                            <input type="hidden" name="id" >
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <br><br>
        <div class="card">
            <table class="table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Telp/ HP</th>
                        <th>Email</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    
    <script>
        
        $("#addNewData").submit(function(event){
            event.preventDefault();
            
            $.ajax({
                type:"post",
                url:"api.php?req=addnew",
                data: new FormData(this),
                dataType:'json',
                contentType:false,
                cache:false,
                processData:false,
                success:function(res){
                    if(res.msg){
                        swal("Berhasil", "Data sukses di tambah", "success");
                        $("input[name=nama]").val("");
                        $("input[name=hp]").val("");
                        $("input[name=email]").val("");
                        loadTable();
                    }
                }
            });

        });

        $("#editData").submit(function(event){
            event.preventDefault();
            $.ajax({
                type:"post",
                url: "api.php?req=editdata",
                data : new FormData(this),
                dataType:"json",
                contentType:false,
                cache:false,
                processData:false,
                success:function(res){
                    if(res.msg){
                        swal("Berhasil", "Data sukses di update", "success");
                        loadTable();
                        $("button[type=reset]").click();
                    }
                }
            });
        });

        function loadTable(){
            $(".table").DataTable().destroy();
            $(".table").DataTable({
                'ajax':"api.php?req=datatable"
            });
        }
        //edit data mahasiswa
        function editData(id){
            $.ajax({
                type:"post",
                url:"api.php?req=onerow",
                data:"id="+ id,
                dataType:"json",
                success:function(res){
                    console.log(res);
                    $("#nama").val(res.nama);
                    $("#hp").val(res.hp);
                    $("#email").val(res.email);
                    $("input[name=id]").val(id);
                }
            });
        }

        //delete data mahasiswa
        function deleteData(id){

            swal({
                title:"Yakin akan menghapus item ini?",
                text:"klik [OK] untuk menghapus item.",
                icon:"warning",
                button:true,
                dangerMode:true
            })
            .then((willDelete) => {
                if(willDelete){
                    $.ajax({
                        type:"post",
                        url:"api.php?req=deletedata",
                        data:"id=" + id,
                        dataType:"json",
                        success:function(res){
                            if(res.msg){
                                swal("Data berhasil di hapus.", {icon:"success"});
                                loadTable();
                            }
                        }
                    });
                }
            });

            
        }

        loadTable();

    </script>

</body>
</html>